import * as React from "react";
import {DumbButton} from "../../../components/forms/elements/DumbButton";

interface ButtonManagerProps {
    name: string;
    buttonAction: () => void;
    value: string;
}

export class ButtonManager extends React.Component<ButtonManagerProps, {}> {

    handleClick() {
        console.log(this.props);
    }

    render() {
        return (
            <div onClick={() => {this.handleClick()}}>
                <DumbButton name={this.props.name} value={this.props.value} />
            </div>
        );
    }
}